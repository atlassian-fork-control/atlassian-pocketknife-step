This library lets you do some fancy things by chaining Either (Atlassian Fugue Either)

Just realize, that can also be this generic (Check TestEitherSteps testcase)
```
    Either<AnError, Double> either = Steps
                    .begin(firstOKOperation())
                    .then(str -> secondNGOperation())
                    .then((str, number) -> thirdOKOperation())
                    .then((str, number, boo) -> firstOKOperation())
                    .yield((str, number, boo, str2) -> new Double(number));
```

Also works for Option (Check TestOptionSteps testcases)
```
    Option<Long> stepped = Steps
                    .begin(firstOKOption(STRING))
                    .then(() -> secondOKOption(STRING))
                    .then((first, second) -> Option.some(first + second))
                    .then(() -> thirdOKOption(STRING))
                    .yield((value1, value2, value3, value4) -> new Long(value3));
```

And also works for Optional (Check TestOptionalSteps test cases)
```
    Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then((first, second) -> Optional.of(first + second))
                .then(() -> thirdOKOptional(STRING))
                .yield((value1, value2, value3, value4) -> new Long(value3));
```

It will only work with Java 8, given we utilize Function, Consumer, Supplier from JDK 8 as well as provide support for Optional.

# Release

The release build is found at https://servicedesk-bamboo.internal.atlassian.com/browse/PKSTE-PKSTERLS