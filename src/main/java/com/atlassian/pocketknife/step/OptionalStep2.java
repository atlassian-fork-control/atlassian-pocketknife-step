package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function2;
import com.atlassian.pocketknife.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

public class OptionalStep2<A, B> extends OptionalStep {

    private final Optional<A> option1;
    private final Optional<B> option2;

    OptionalStep2(Optional<A> option1, Optional<B> option2) {
        this.option1 = option1;
        this.option2 = option2;
    }

    public <C> OptionalStep3<A, B, C> then(Function2<A, B, Optional<C>> functor) {
        Optional<C> option3 = option1.flatMap(e1 -> option2.flatMap(e2 -> functor.apply(e1, e2)));
        return new OptionalStep3<>(option1, option2, option3);
    }

    public <C> OptionalStep3<A, B, C> then(Supplier<Optional<C>> functor) {
        Optional<C> Optional = option1.flatMap(e1 -> option2.flatMap(e2 -> functor.get()));
        return new OptionalStep3<>(option1, option2, Optional);
    }

    public <Z> Optional<Z> yield(Function2<A, B, Z> function2) {
        return option1.flatMap(e1 -> option2.map(e2 -> function2.apply(e1, e2)));
    }

}
