package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function5;
import com.atlassian.pocketknife.step.ops.OptionalStep;

import java.util.Optional;

public class OptionalStep5<A, B, C, D, E> extends OptionalStep {
    private final Optional<A> option1;
    private final Optional<B> option2;
    private final Optional<C> option3;
    private final Optional<D> option4;
    private final Optional<E> option5;

    OptionalStep5(Optional<A> option1, Optional<B> option2, Optional<C> option3, Optional<D> option4, Optional<E> option5) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.option5 = option5;
    }

    public <Z> Optional<Z> yield(Function5<A, B, C, D, E, Z> functor) {
        return option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> option5.map(
                                                e5 -> functor.apply(e1, e2, e3, e4, e5))))));
    }
}
