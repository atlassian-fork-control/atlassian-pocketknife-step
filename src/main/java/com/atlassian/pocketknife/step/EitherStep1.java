package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function1;
import com.atlassian.pocketknife.step.ops.EitherStep;
import io.atlassian.fugue.Either;

import java.util.function.Supplier;

public class EitherStep1<E1, E> extends EitherStep {

    private final Either<E, E1> either1;

    EitherStep1(Either<E, E1> either1) {
        this.either1 = either1;
    }

    public <E2> EitherStep2<E1, E2, E> then(Function1<E1, Either<E, E2>> function1) {
        Either<E, E2> either2 = either1.flatMap(function1::apply);
        return new EitherStep2<>(either1, either2);
    }

    public <E2> EitherStep2<E1, E2, E> then(Supplier<Either<E, E2>> supplier) {
        Either<E, E2> either2 = either1.flatMap(e1 -> supplier.get());
        return new EitherStep2<>(either1, either2);
    }

    public <Z> Either<E, Z> yield(Function1<E1, Z> function1) {
        return either1.map(function1::apply);
    }

}
