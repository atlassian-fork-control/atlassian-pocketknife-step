package com.atlassian.pocketknife.step;

import io.atlassian.fugue.Either;
import org.junit.Assert;
import org.junit.Test;

import static com.atlassian.pocketknife.step.NewerSupport.fifthNGEither;
import static com.atlassian.pocketknife.step.NewerSupport.fifthOKEither;
import static com.atlassian.pocketknife.step.NewerSupport.firstNGEither;
import static com.atlassian.pocketknife.step.NewerSupport.firstOKEither;
import static com.atlassian.pocketknife.step.NewerSupport.fourthNGEither;
import static com.atlassian.pocketknife.step.NewerSupport.fourthOKEither;
import static com.atlassian.pocketknife.step.NewerSupport.secondNGEither;
import static com.atlassian.pocketknife.step.NewerSupport.secondOKEither;
import static com.atlassian.pocketknife.step.NewerSupport.thirdNGEither;
import static com.atlassian.pocketknife.step.NewerSupport.thirdOKEither;

public class TestEitherSteps {

    private static final String STRING = "123456";
    private static final String STRING_UPPERED = "QWERTY";
    private static final String STRING_LOWERED = "qwerty";
    private static final Long LONG = 123456L;
    private static final Long LONGLONG = 123456123456L;

    @Test
    public void test_1_step_success() {

        Either<AnError, Long> stepped = Steps
                .begin(firstOKEither(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be 123456", stepped.right().get().equals(LONG));
    }

    @Test
    public void test_1_step_failure() {
        Either<AnError, Long> stepped = Steps
                .begin(firstNGEither(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be FirstError", stepped.left().get() == AnError.FIRST.ERROR);
    }

    @Test
    public void test_2_step_success() {
        Either<AnError, Long> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .yield((str, integer) -> new Long(str + integer));

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(LONGLONG));
    }

    @Test
    public void test_2_step_failure() {
        Either<AnError, Long> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondNGEither(LONG))
                .yield((str, integer) -> new Long(str + integer));

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be SecondError", stepped.left().get() == AnError.SECOND.ERROR);
    }

    @Test
    public void test_3_step_success() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdOKEither(true))
                .yield((string, integer, boo) -> string + integer + boo);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false));
    }

    @Test
    public void test_3_step_failure() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdNGEither(true))
                .yield((string, integer, boo) -> string + integer + boo);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.THIRD.ERROR);
    }

    @Test
    public void test_4_step_success() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdOKEither(true))
                .then((string, aLong, aBoolean) -> fourthOKEither(STRING_UPPERED))
                .yield((string, integer, boo, string2) -> string + integer + boo + string2);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false + STRING_LOWERED));
    }

    @Test
    public void test_4_step_failure() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdOKEither(true))
                .then((string, aLong, aBoolean) -> fourthNGEither(STRING_UPPERED))
                .yield((string, integer, boo, string2) -> string + integer + boo + string2);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.FORTH.ERROR);
    }

    @Test
    public void test_5_step_success() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdOKEither(true))
                .then((string, aLong, aBoolean) -> fourthOKEither(STRING_UPPERED))
                .then((string, aLong, aBoolean, string2) -> fifthOKEither(LONG))
                .yield((string, integer, boo, string2, ll) -> string + integer + boo + string2 + ll);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false + STRING_LOWERED + (LONG / 2)));
    }

    @Test
    public void test_5_step_failure() {
        Either<AnError, String> stepped = Steps.begin(firstOKEither(STRING))
                .then(str -> secondOKEither(LONG))
                .then((string, aLong) -> thirdOKEither(true))
                .then((string, aLong, aBoolean) -> fourthOKEither(STRING_UPPERED))
                .then((string, aLong, aBoolean, string2) -> fifthNGEither(LONG))
                .yield((string, integer, boo, string2, ll) -> string + integer + boo + string2 + ll);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.FIFTH.ERROR);
    }

}
